from setuptools import setup, find_packages

setup(
    name='model-covid-19',
    version='0.1.11',
    description='models for COVID19',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)
